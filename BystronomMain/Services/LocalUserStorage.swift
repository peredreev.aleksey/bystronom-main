//
//  LocalUserStorage.swift
//  BystronomMain
//
//  Created by Alex Peredreev on 27.07.2021.
//

import Foundation

enum UserDefaultsKeys: String {
    case token
    case refresh
    case guestPreferencies
    case firstLaunch
}

struct LocalUserStorage {

    private static let defaults = UserDefaults.standard

    static var accessToken: String? {
        get { defaults.string(forKey: UserDefaultsKeys.token.rawValue) }
        set { defaults.setValue(newValue, forKey: UserDefaultsKeys.token.rawValue) }
    }

    static var refreshToken: String? {
        get { defaults.string(forKey: UserDefaultsKeys.refresh.rawValue) }
        set { defaults.setValue(newValue, forKey: UserDefaultsKeys.refresh.rawValue) }
    }

    static var guestPreferencies: Bool {
        get { defaults.bool(forKey: UserDefaultsKeys.guestPreferencies.rawValue) }
        set { defaults.setValue(newValue, forKey: UserDefaultsKeys.guestPreferencies.rawValue) }
    }

    static var firstLaunch: Bool {
        get { defaults.bool(forKey: UserDefaultsKeys.firstLaunch.rawValue) }
        set { defaults.setValue(newValue, forKey: UserDefaultsKeys.firstLaunch.rawValue) }
    }

}
