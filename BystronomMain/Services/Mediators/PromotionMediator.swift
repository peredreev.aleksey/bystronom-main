//
//  PromotionMediator.swift
//  BystronomMain
//
//  Created by Alex Peredreev on 27.07.2021.
//

import Foundation
import RxSwift
import RxCocoa

protocol PromotionFetchable {
    func fetchPromotions(on event: Observable<(page: Int, limit: Int)>,
                         processing: BehaviorRelay<Bool>) -> Observable<[Promotion]>

    func fetchPromotion(on event: Observable<Int>,
                        processing: BehaviorRelay<Bool>) -> Observable<Promotion>
}


struct PromotionMediator {

    let service: PromotionService

    init(service: PromotionService) {
        self.service = service
    }

    func fetchPromotions(on event: Observable<(page: Int, limit: Int)>,
                         processing: BehaviorRelay<Bool>) -> Observable<[Promotion]> {

        event
            .do(onNext: { _ in processing.accept(true) })
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .userInitiated))
            .flatMapLatest { pagination -> Observable<[Promotion]> in
                service.fetchPromotions(page: pagination.page, limit: pagination.limit)
                    .asObservable()
                    .eventHandle(processing: processing)
            }
            .do(onNext: { _ in processing.accept(false) })
    }

    func fetchPromotion(on event: Observable<Int>,
                        processing: BehaviorRelay<Bool>) -> Observable<Promotion> {

        event
            .do(onNext: { _ in processing.accept(true) })
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .userInitiated))
            .flatMapLatest { id -> Observable<Promotion> in
                service.fetchPromotion(by: id)
                    .asObservable()
                    .eventHandle(processing: processing)
            }
            .do(onNext: { _ in processing.accept(false) })
    }
}

extension PromotionMediator: PromotionFetchable {}
