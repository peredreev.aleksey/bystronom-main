//
//  GeneralMediator.swift
//  BystronomMain
//
//  Created by Alex Peredreev on 27.07.2021.
//

import Foundation
import RxSwift
import RxCocoa

protocol GeneralMediator {

    func fetchGuestToken(on event: Observable<Void>,
                         processing: BehaviorRelay<Bool>) -> Observable<Token>
}

struct GeneralMediatorImp {

    let service: GeneralService

    init(service: GeneralService) {
        self.service = service
    }
}

extension GeneralMediatorImp {

    func fetchGuestToken(on event: Observable<Void>,
                         processing: BehaviorRelay<Bool>) -> Observable<Token> {

        event
            .flatMapLatest { _ -> Observable<Token> in
                service.fetchGuest()
                    .asObservable()
                    .eventHandle(processing: processing)
            }
            .do(onNext: { _ in processing.accept(false) })
    }
}

extension GeneralMediatorImp: GeneralMediator {}
