//
//  MediatorServiceContainer.swift
//  BystronomMain
//
//  Created by Alex Peredreev on 27.07.2021.
//

import Foundation

struct MediatorServiceContainer {
    let generalMediator: GeneralMediator
    let promotionMediator: PromotionMediator
}
