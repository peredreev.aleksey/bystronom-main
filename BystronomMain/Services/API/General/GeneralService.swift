//
//  GeneralService.swift
//  BystronomMain
//
//  Created by Alex Peredreev on 13.07.2021.
//

import Foundation
import RxSwift
import Moya

enum ApiError: Error {
    case badRequest
    case refresh
}

struct GeneralService {
    
    let provider = MasterProvider<GeneralAPI>()
    private let testProvider = MasterProvider<GeneralAPI>.init(stubClosure: MasterProvider<GeneralAPI>.immediatelyStub(_:))

}

extension GeneralService {
    
    func fetchGuest() -> Single<Token> {
        provider.rx.request(.fetchGuest)
            .filterSuccessfulStatusAndRedirectCodes()
            .map(ServerResponse<ServerToken>.self)
            .map { TokenMapper().toLocal(serverEntity: $0.data) }

//            .subscribeOn(<#T##scheduler: ImmediateSchedulerType##ImmediateSchedulerType#>)
        
    }
}

struct ServerUser: Decodable {
    let name: String?
    let age: Int?
}

struct User {
    let name: String
    let age: Int
}

final class UserModelMapper: BaseModelMapper<ServerUser, User> {
    
    override func toLocal(serverEntity: ServerUser) -> User {
        User(name: serverEntity.name.orEmpty(), age: serverEntity.age ?? 0)
    }
}

extension Optional where Wrapped == String {
    
    func orEmpty() -> String {
        if let value = self {
            return value
        } else {
            return ""
        }
    }
}
