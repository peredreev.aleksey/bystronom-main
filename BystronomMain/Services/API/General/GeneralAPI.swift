//
//  GeneralAPI.swift
//  BystronomMain
//
//  Created by Alex Peredreev on 13.07.2021.
//

import Foundation
import Moya

enum GeneralAPI {
    case fetchGuest
    case fetchPromotions(page: Int, limit: Int)
    case getUser
}

extension GeneralAPI: TargetType {
    var baseURL: URL {
        return Consts.API.baseUrl
    }
    
    var path: String {
        switch self {
        case .fetchGuest:
            return "/api/v1/security/login/guest"
        case .fetchPromotions:
            return "/api/v1/promotions"
        case .getUser:
            return "/api/v1/user"

        }
    }
    
    var method: Moya.Method {
        switch self {
        case .fetchGuest:
            return .post
        case .fetchPromotions:
            return .get
        case .getUser:
            return .get
        }
    }
    
    var sampleData: Data {
        switch self {
        case .getUser:
            return Bundle().json(by: "user")
        default:
            return Data()
        }
    }
    
    var task: Task {
        var params = [String: Any]()
        
        switch self {
        case .fetchGuest:
            params["pushToken"] = "dasdasda"
            params["cityId"] = 1
            params["shopId"] = 1
            params["deviceType"] = "ios|android"
            
            return .requestCompositeParameters(bodyParameters: params,
                                               bodyEncoding: JSONEncoding.default,
                                               urlParameters: [:])
        case let .fetchPromotions(page: page, limit: limit):
            params["p[page]"] = page
            params["p[amount]"] = limit
            
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
        case .getUser:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        return nil
    }
    
    
}
