//
//  PromotionAPI.swift
//  BystronomMain
//
//  Created by Alex Peredreev on 27.07.2021.
//

import Foundation
import Moya

enum PromotionAPI {
    case fetchPromotions(page: Int, limit: Int)
    case fetchPromotion(by: Int)
}

extension PromotionAPI: TargetType {
    var baseURL: URL {
        return Consts.API.baseUrl
    }

    var path: String {
        switch self {
        case .fetchPromotions:
            return "/api/v1/promotions"
        case let .fetchPromotion(by: id):
            return "/api/v1/promotions/\(id)"
        }
    }

    var method: Moya.Method {
        switch self {
        case .fetchPromotions, .fetchPromotion:
            return .get
        }
    }

    var sampleData: Data {
        return Data()
    }

    var task: Task {
        var params = [String: Any]()

        switch self {
        case .fetchPromotion:
            return .requestPlain

        case let .fetchPromotions(page: page, limit: limit):
            params["p[page]"] = page
            params["p[amount]"] = limit

            return .requestParameters(parameters: params,
                                      encoding: URLEncoding.default)
        }
    }

    var headers: [String : String]? {
        return nil
    }


}
