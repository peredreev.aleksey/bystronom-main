//
//  PromotionService.swift
//  BystronomMain
//
//  Created by Alex Peredreev on 27.07.2021.
//

import Foundation
import RxSwift

struct PromotionService {

    let provider = MasterProvider<PromotionAPI>()
}

extension PromotionService {

    func fetchPromotions(page: Int, limit: Int) -> Single<[Promotion]> {
        provider.rx.request(.fetchPromotions(page: page, limit: limit))
            .filterSuccessfulStatusAndRedirectCodes()
            .map(ServerArrayResponse<ServerPromotion>.self)
            .map { PromotionMapper().toLocal(list: $0.data) }
    }

    func fetchPromotion(by id: Int) -> Single<Promotion> {
        provider.rx.request(.fetchPromotion(by: id))
            .filterSuccessfulStatusAndRedirectCodes()
            .map(ServerResponse<ServerPromotion>.self)
            .map { PromotionMapper().toLocal(serverEntity: $0.data) }
    }
}
