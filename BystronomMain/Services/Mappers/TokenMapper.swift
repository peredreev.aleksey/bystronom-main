//
//  TokenMapper.swift
//  BystronomMain
//
//  Created by Alex Peredreev on 13.07.2021.
//

import Foundation

final class TokenMapper: BaseModelMapper<ServerToken, Token> {
    
    override func toLocal(serverEntity: ServerToken) -> Token {
        
        let model = Token(token: serverEntity.accessToken ?? "")
        
        return model
    }
}
