//
//  PromotionMapper.swift
//  BystronomMain
//
//  Created by Alex Peredreev on 27.07.2021.
//

import Foundation

final class PromotionMapper: BaseModelMapper<ServerPromotion, Promotion> {

    override func toLocal(serverEntity: ServerPromotion) -> Promotion {

        let image = URL(string: serverEntity.imagesUrl ?? "")
        let beginDate = (serverEntity.startPromotion ?? "").toDate()
        let endDate = (serverEntity.endPromotion ?? "").toDate()
        let fullDescriptionUrl = URL(string: serverEntity.fullRulesUrl ?? "")

        let model = Promotion(id: serverEntity.id ?? 0,
                              image: image,
                              title: serverEntity.name ?? "",
                              beginDate: beginDate,
                              endDate: endDate,
                              description: serverEntity.rules ?? "",
                              fullDescriptionUrl: fullDescriptionUrl)

        return model
    }
}
