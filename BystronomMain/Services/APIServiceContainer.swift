//
//  APIServiceContainer.swift
//  BystronomMain
//
//  Created by Alex Peredreev on 17.07.2021.
//

import Foundation

struct APIServiceContainer {
    let generalService: GeneralService
    let promotionService: PromotionService
}
