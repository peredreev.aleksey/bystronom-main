//
//  Constants.swift
//  BystronomMain
//
//  Created by Alex Peredreev on 13.07.2021.
//

import Foundation

enum Consts {
    
    enum API {
        static let baseUrl = URL(string: "https://beta-bystronom.profsoft.online")!
        
        static let authorizationHeader = "apiKey"
        static let xContentTypeName = "Content-type"
        static let xContentTypeValue = "application/json"
        static let authVerbose = true
        static let backendVerbose = true
    }
}
