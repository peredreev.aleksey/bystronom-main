//
//  ObservableType+Extensions.swift
//  BystronomMain
//
//  Created by Alex Peredreev on 27.07.2021.
//

import Foundation
import RxSwift
import RxCocoa
import Moya

extension ObservableType {
    func eventHandle(processing: BehaviorRelay<Bool>) -> Observable<Element> {
        self.materialize()
            .map { event -> Event<Element> in
                if case let .error(error) = event {
                    if let _ = error as? ApiError {
                        processing.accept(false)
                    } else if let moyaError = error as? MoyaError {
                        switch moyaError {
                        case .underlying:
                            processing.accept(false)
                        default:
                            processing.accept(false)
                        }
                    }
                    print(error)
                    return.completed
                } else {
                    return event
                }
            }
            .dematerialize()
    }
}
