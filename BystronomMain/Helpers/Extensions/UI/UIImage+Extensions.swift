//
//  UIImage+Extensions.swift
//  BystronomMain
//
//  Created by Alex Peredreev on 20.07.2021.
//

import Foundation
import Kingfisher

extension UIImageView {
    func loadImageByUrl(_ url: URL?, placeholder: UIImage? = nil) {
        self.kf.indicatorType = .none
        self.kf.setImage(with: url,
                         placeholder: placeholder,
                         options: [.transition(.fade(0.3)),
                                   .cacheOriginalImage]) { result in
            switch result {
            case .success(let value):
                print("Task done for: \(value.source.url?.absoluteString ?? "")")
            case .failure(let error):
                print("Job failed: \(error.localizedDescription)")
            }
        }
    }
}
