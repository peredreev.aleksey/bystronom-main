//
//  Bundle+Extensions.swift
//  BystronomMain
//
//  Created by Alex Peredreev on 27.07.2021.
//

import Foundation

extension Bundle {
    
    func json(by name: String) -> Data {
        guard let path = path(forResource: name, ofType: "json") else { return Data() }
        
        let data = try! Data(contentsOf: URL(fileURLWithPath: path))
        
        return data
        
    }
}
