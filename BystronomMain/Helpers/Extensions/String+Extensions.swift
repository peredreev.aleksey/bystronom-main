//
//  String+Extensions.swift
//  BystronomMain
//
//  Created by Alex Peredreev on 27.07.2021.
//

import Foundation

extension String {
    func toDate() -> Date? {
        let dateFormatter = ISO8601DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.formatOptions = .withFullDate

        let date = dateFormatter.date(from: self)
        return date
    }
}
