//
//  GeneralScreenViewController.swift
//  BystronomMain
//
//  Created by Alex Peredreev on 16.07.2021.
//

import UIKit
import RxSwift
import RxCocoa
import Reusable
import Then

final class GeneralScreenViewController: UIViewController, StoryboardBased {
    
    // MARK: - Properties
    
    private let disposeBag = DisposeBag()
    
    private var input: GeneralScreenViewModelInputProtocol!
    private var output: GeneralScreenViewModelOutputProtocol!
    private var viewModel: GeneralScreenViewModelProtocol!
    
    private let onAppear = PublishRelay<Void>()
    
    // MARK: - Outlets
    
    @IBOutlet private weak var tableView: UITableView!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
        bindUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        onAppear.accept(())
    }
    
    // MARK: - Public Methods
    
    func inject(_ viewModel: GeneralScreenViewModelProtocol) {
        self.viewModel = viewModel
    }
    
}

// MARK: - Private Methods

private extension GeneralScreenViewController {
    
    func configureUI() {
        configureViews()
        configureTableView()
    }
    
    func configureViews() {
        navigationController?.navigationBar.isHidden = true
    }
    
    func configureTableView() {
        tableView.do {
            $0.register(cellType: ShopAddressTableViewCell.self)
            $0.register(cellType: ActionCarouselTableViewCell.self)
            $0.separatorStyle = .none
            $0.showsVerticalScrollIndicator = false
            $0.tableFooterView = UIView()
            $0.alwaysBounceVertical = false
            $0.bounces = false
        }
        
    }
    
}

private extension GeneralScreenViewController {
    
    func bindUI() {
        bindViewModel()
        bindTableView()
    }
    
    func bindViewModel() {
     
        let input = GeneralScreenViewModelInput(onAppear: onAppear.asObservable())
        
        output = viewModel.transform(input: input)
    }
    
    func bindTableView() {
        output.sections.drive(tableView.rx.items(dataSource: output.dataSource)).disposed(by: disposeBag)
    }
}
