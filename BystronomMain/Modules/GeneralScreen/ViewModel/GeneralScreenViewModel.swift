//
//  GeneralScreenViewModel.swift
//  BystronomMain
//
//  Created by Alex Peredreev on 16.07.2021.
//

import Foundation
import RxCocoa
import RxSwift
import RxFlow

struct GeneralScreenViewModelInput: GeneralScreenViewModelInputProtocol {
    
    let onAppear: Observable<Void>
    
}

struct GeneralScreenViewModelOutput: GeneralScreenViewModelOutputProtocol {
    
    let dataSource = GeneralScreenDataSource().dataSource
    
    let sections: Driver<[GeneralScreenDataSource.SectionType]>
    
}

final class GeneralScreenViewModel: Stepper {
    
    let mediator: GeneralMediator
    let promotionMediator: PromotionFetchable
    let disposeBag = DisposeBag()
    var steps = PublishRelay<Step>()

    internal init(mediator: GeneralMediator, promotionMediator: PromotionFetchable) {
        self.mediator = mediator
        self.promotionMediator = promotionMediator
    }
}

extension GeneralScreenViewModel: GeneralScreenViewModelProtocol {
    
    typealias SectionType = GeneralScreenDataSource.SectionType
    
    func transform(input: GeneralScreenViewModelInputProtocol) -> GeneralScreenViewModelOutputProtocol {
        
        let processing = BehaviorRelay<Bool>(value: true)
        let sections = BehaviorRelay<[SectionType]>(value: [])
        let mockItems = BehaviorRelay<[Int]>(value: [])
        let firstLaunch = BehaviorRelay<Bool>(value: LocalUserStorage.firstLaunch)
        let promotionPage = BehaviorRelay<Int>(value: 1)
        let promotionLimit = BehaviorRelay<Int>(value: 5)
        
        let selectedPromotion = PublishRelay<ActionCarouselCellModelProtocol>()
        
        let readyToFetchGuestToken = PublishRelay<Void>()
        let readyToFetchGeneralScreenData = PublishRelay<Void>()
        
        input.onAppear
            .withLatestFrom(firstLaunch)
            .filter { !$0 }
            .map { _ in Void() }
            .bind(to: readyToFetchGuestToken)
            .disposed(by: disposeBag)
        
        input.onAppear
            .withLatestFrom(firstLaunch)
            .filter { $0 }
            .map { _ in Void() }
            .bind(to: readyToFetchGeneralScreenData)
            .disposed(by: disposeBag)
        
        mediator.fetchGuestToken(on: readyToFetchGuestToken.asObservable(),
                                 processing: processing)
            .do(onNext: { data in
                LocalUserStorage.accessToken = data.token
                LocalUserStorage.firstLaunch = true
                firstLaunch.accept(true)
            })
            .map { _ in Void() }
            .bind(to: readyToFetchGeneralScreenData)
            .disposed(by: disposeBag)
        
        let fetchPromotionEvent = readyToFetchGeneralScreenData
            .withLatestFrom(promotionPage)
            .withLatestFrom(promotionLimit) { (page: $0, limit: $1) }
        
        promotionMediator.fetchPromotions(on: fetchPromotionEvent,
                                          processing: processing)
            .map { GeneralScreenBuilder.build(promotions: $0,
                                              selectedPromotion: selectedPromotion) }
            .bind(to: sections)
            .disposed(by: disposeBag)
        
        let output = GeneralScreenViewModelOutput(sections: sections.asDriver(onErrorJustReturn: []))
        
        return output
    }
    
}
