//
//  GeneralScreenViewModelProtocols.swift
//  BystronomMain
//
//  Created by Alex Peredreev on 16.07.2021.
//

import Foundation
import RxCocoa
import RxSwift

protocol GeneralScreenViewModelInputProtocol {
    var onAppear: Observable<Void> { get }
}

protocol GeneralScreenViewModelOutputProtocol {
 
    var dataSource: GeneralScreenDataSource.DataSource { get }
    var sections: Driver<[GeneralScreenDataSource.SectionType]> { get }
}

protocol GeneralScreenViewModelProtocol {
    func transform(input: GeneralScreenViewModelInputProtocol) -> GeneralScreenViewModelOutputProtocol
}
