//
//  GeneralScreenBuilder.swift
//  BystronomMain
//
//  Created by Alex Peredreev on 16.07.2021.
//

import Foundation
import RxCocoa

struct GeneralScreenBuilder {
    
    typealias SectionType = GeneralScreenDataSource.SectionType
    
    static func build(promotions: [Promotion], selectedPromotion: PublishRelay<ActionCarouselCellModelProtocol>) -> [SectionType] {
        
        var items = [GeneralScreenItem]() // TODO: put items here * add CellModel by JSON
        
        let promotions = GeneralScreenItem.actions(model: promotions, selectedModel: selectedPromotion)
        
        items.append(promotions)

        return [SectionType.init(model: GeneralScreenHeader.search, items: items)]
    }
}
