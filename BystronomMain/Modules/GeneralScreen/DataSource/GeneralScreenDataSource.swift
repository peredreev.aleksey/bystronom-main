//
//  GeneralScreenDataSource.swift
//  BystronomMain
//
//  Created by Alex Peredreev on 16.07.2021.
//

import UIKit
import RxDataSources
import RxCocoa

enum GeneralScreenHeader {
    case search
}

enum GeneralScreenItem {
    case address(model: ShopAddressCellModelProtocol)
    case loyalityCard
    case category
    case clubs
    case blockTitle
    case actions(model: [ActionCarouselCellModelProtocol], selectedModel: PublishRelay<ActionCarouselCellModelProtocol>)
    case products
}

final class GeneralScreenDataSource {
    
    typealias SectionType = SectionModel<GeneralScreenHeader, GeneralScreenItem>
    
    typealias DataSource = RxTableViewSectionedReloadDataSource<SectionType>
    
    typealias CellViewModelBuilder = CellCollectionViewModelBuilder<ActionCarouselTableViewCell, ActionCarouselTableViewCellViewModel>
    
    let dataSource = DataSource { dataSource, tableView, indexPath, _ -> UITableViewCell in
        switch dataSource[indexPath] {
        case let .address(model: model):
            return CellBuilder<ShopAddressTableViewCell>.build(tableView: tableView,
                                                               indexPath: indexPath,
                                                               model: model)
        case let .actions(model: model, selectedModel: selectedModel):
            return CellViewModelBuilder.build(tableView: tableView,
                                              indexPath: indexPath,
                                              model: model,
                                              selectedModel: selectedModel)
        default:
            return UITableViewCell()
        }
    }
}
