//
//  AppDelegate.swift
//  BystronomMain
//
//  Created by Alex Peredreev on 13.07.2021.
//

import UIKit
import RxFlow
import RxSwift
import RxCocoa

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var appFlow: AppFlow!
    let coordinator = FlowCoordinator()
    private let disposeBag = DisposeBag()
    private var serviceContainer: APIServiceContainer!
    private var mediatorContainer: MediatorServiceContainer!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        guard let window = self.window else { return false }

        serviceContainer = buildServiceContainer()
        mediatorContainer = buildMediatorContainer()

        self.appFlow = AppFlow(withWindow: window, serviceContainer: serviceContainer, mediatorContainer: mediatorContainer)

        coordinator.rx.didNavigate
            .subscribe(onNext: { flow, step in
                print("did navigate to flow=\(flow) and step=\(step)")
            }).disposed(by: disposeBag)

        coordinator.coordinate(flow: appFlow, with: AppStepper())

        return true
    }

}

private extension AppDelegate {
    func buildServiceContainer() -> APIServiceContainer {
        let generalService = GeneralService()
        let promotionService = PromotionService()
        return APIServiceContainer(generalService: generalService,
                                   promotionService: promotionService)
    }
    
    func buildMediatorContainer() -> MediatorServiceContainer {
        let generalMediator = GeneralMediatorImp(service: serviceContainer.generalService)
        let promotionMediator = PromotionMediator(service: serviceContainer.promotionService)
        return MediatorServiceContainer(generalMediator: generalMediator,
                                        promotionMediator: promotionMediator)
    }
}
