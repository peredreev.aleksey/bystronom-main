//
//  ShopAddressCellModel.swift
//  BystronomMain
//
//  Created by Alex Peredreev on 20.07.2021.
//

import Foundation
import RxCocoa

protocol ShopAddressCellModelProtocol {
    var title: String { get }
    var addressTap: PublishRelay<Void> { get }
}

struct ShopAddressCellModel {
    let title: String
    let addressTap: PublishRelay<Void>
}

extension ShopAddressCellModel: ShopAddressCellModelProtocol {
    
}
