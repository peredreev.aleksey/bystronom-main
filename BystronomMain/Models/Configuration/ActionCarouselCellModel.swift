//
//  ActionCarouselCellModel.swift
//  BystronomMain
//
//  Created by Alex Peredreev on 27.07.2021.
//

import Foundation

protocol ActionCarouselCellModelProtocol {
    var title: String { get }
    var image: URL? { get }
}

struct ActionCarouselCellModel: ActionCarouselCellModelProtocol {
    var id: Int
    var title: String
    var image: URL?
}
