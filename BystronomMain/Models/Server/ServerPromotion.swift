//
//  ServerPromotion.swift
//  BystronomMain
//
//  Created by Alex Peredreev on 27.07.2021.
//

import Foundation

struct ServerPromotion: Decodable {
    let id: Int?
    let name: String?
    let startPromotion: String?
    let endPromotion: String?
    let rules: String?
    let fullRulesUrl: String?
    let imagesUrl: String?
}
