//
//  ServerToken.swift
//  BystronomMain
//
//  Created by Alex Peredreev on 13.07.2021.
//

import Foundation

struct ServerToken: Decodable {
    let accessToken: String?
    let refreshToken: String?
    let isGuestUser: Bool
}
