//
//  Token.swift
//  BystronomMain
//
//  Created by Alex Peredreev on 13.07.2021.
//

import Foundation

struct Token {
    let token: String
}
