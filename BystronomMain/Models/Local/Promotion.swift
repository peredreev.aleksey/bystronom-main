//
//  Promotion.swift
//  BystronomMain
//
//  Created by Alex Peredreev on 27.07.2021.
//

import Foundation

protocol PromotionCellModelProtocol {
    var id: Int { get }
    var image: URL? { get }
    var title: String { get }
    var beginDate: Date? { get }
    var endDate: Date? { get }
    var beginDateString: String { get }
    var endDateString: String { get }
    var description: String { get }
}

struct Promotion {
    let id: Int
    let image: URL?
    let title: String
    let beginDate: Date?
    let endDate: Date?
    let description: String
    let fullDescriptionUrl: URL?
}

extension Promotion: ActionCarouselCellModelProtocol {}

extension Promotion: PromotionCellModelProtocol {
    var beginDateString: String {
        guard let date = beginDate else { return "" }
        return DateFormatters.fullDate.string(from: date)
    }

    var endDateString: String {
        guard let date = endDate else { return "" }
        return DateFormatters.fullDate.string(from: date)
    }
}
