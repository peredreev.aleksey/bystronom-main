//
//  GeneralFlow.swift
//  BystronomMain
//
//  Created by Alex Peredreev on 17.07.2021.
//

import UIKit
import RxFlow
import RxCocoa

final class GeneralFlow: Flow {

    var root: Presentable {
        return self.rootViewController
    }

    private lazy var rootViewController: UINavigationController = {
        let viewController = UINavigationController()
        return viewController
    }()

    private let serviceContainer: APIServiceContainer
    private let mediatorContainer: MediatorServiceContainer

    init(serviceContainer: APIServiceContainer, mediatorContainer: MediatorServiceContainer) {
        self.serviceContainer = serviceContainer
        self.mediatorContainer = mediatorContainer
    }

    deinit {
        print("\(type(of: self)): \(#function)")
    }

    func navigate(to step: Step) -> FlowContributors {
        guard let step = step as? AppStep else { return .none }

        switch step {
        case .general:
            return navigationToGeneralScreen()
        default:
            return .none
        }
    }


    private func navigationToGeneralScreen() -> FlowContributors {
        let viewController = GeneralScreenViewController.instantiate()
        let viewModel = GeneralScreenViewModel(mediator: mediatorContainer.generalMediator, promotionMediator: mediatorContainer.promotionMediator)
        viewController.inject(viewModel)

        rootViewController.pushViewController(viewController, animated: true)

        return .one(flowContributor: .contribute(withNextPresentable: viewController,
                                                 withNextStepper: viewModel))
    }
}


