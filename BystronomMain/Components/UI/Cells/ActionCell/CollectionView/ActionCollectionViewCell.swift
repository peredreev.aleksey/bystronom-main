//
//  ActionCollectionViewCell.swift
//  BystronomMain
//
//  Created by Alex Peredreev on 20.07.2021.
//

import UIKit


final class ActionCollectionViewCell: UICollectionViewCell, CellConfigurable {

    @IBOutlet weak var actionImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        configureUI()
    }

    func configureCell(with model: ActionCarouselCellModelProtocol) {
        actionImageView.loadImageByUrl(model.image)
    }
}

private extension ActionCollectionViewCell {
    
    func configureUI() {
        configureImage()
    }
    
    func configureImage() {
        actionImageView.contentMode = .scaleAspectFill
    }
}
