//
//  ActionCarouselTableViewCellViewModel.swift
//  BystronomMain
//
//  Created by Alex Peredreev on 20.07.2021.
//

import Foundation
import RxSwift
import RxCocoa

struct ActionCarouselTableViewCellViewModelInput: ActionCarouselTableViewCellViewModelInputProtocol {
    var selectedModel: Observable<ActionCarouselCellModelProtocol>
    
    
}

struct ActionCarouselTableViewCellViewModelOutput: ActionCarouselTableViewCellViewModelOutputProtocol {
    let items: Driver<[ActionCarouselCellModelProtocol]>
}

final class ActionCarouselTableViewCellViewModel: CollectionViewModable {
    
    let disposeBag = DisposeBag()
    let model = BehaviorRelay<[ActionCarouselCellModelProtocol]>(value: [])
    let selectedModel = PublishRelay<ActionCarouselCellModelProtocol>()
    
    init(model: [ActionCarouselCellModelProtocol], selectedModel: PublishRelay<ActionCarouselCellModelProtocol>) {
        self.model.accept(model)
        self.selectedModel.bind(to: selectedModel).disposed(by: disposeBag)
    }
    
}

extension ActionCarouselTableViewCellViewModel: ActionCarouselTableViewCellViewModelProtocol {
    
    func transform(input: ActionCarouselTableViewCellViewModelInputProtocol) -> ActionCarouselTableViewCellViewModelOutputProtocol {
        
        input.selectedModel
            .bind(to: self.selectedModel)
            .disposed(by: disposeBag)
        
        let output = ActionCarouselTableViewCellViewModelOutput(items: model.asDriver(onErrorJustReturn: []))
        
        return output
    }
}

