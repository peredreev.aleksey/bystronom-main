//
//  ActionCarouselTableViewCellViewModelProtocols.swift
//  BystronomMain
//
//  Created by Alex Peredreev on 20.07.2021.
//

import Foundation
import RxSwift
import RxCocoa

protocol ActionCarouselTableViewCellViewModelInputProtocol {
    var selectedModel: Observable<ActionCarouselCellModelProtocol> { get }
}

protocol ActionCarouselTableViewCellViewModelOutputProtocol {
    var items: Driver<[ActionCarouselCellModelProtocol]> { get }
}

protocol ActionCarouselTableViewCellViewModelProtocol {
    
    func transform(input: ActionCarouselTableViewCellViewModelInputProtocol) -> ActionCarouselTableViewCellViewModelOutputProtocol
}
