//
//  ActionCarouselTableViewCell.swift
//  BystronomMain
//
//  Created by Alex Peredreev on 20.07.2021.
//

import UIKit
import RxSwift
import RxCocoa

class ActionCarouselTableViewCell: UITableViewCell, CellViewModelConfigurable {

    private var disposeBag = DisposeBag()
    private var input: ActionCarouselTableViewCellViewModelInputProtocol!
    private var output: ActionCarouselTableViewCellViewModelOutputProtocol!
    private var viewModel: ActionCarouselTableViewCellViewModelProtocol! {
        didSet {
            disposeBag = DisposeBag()
            configureUI()
            bindUI()
        }
    }

    @IBOutlet private weak var collectionView: UICollectionView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }

    func configureCell(with viewModel: ActionCarouselTableViewCellViewModelProtocol) {
        self.viewModel = viewModel
    }
}

private extension ActionCarouselTableViewCell {
    
    func configureUI() {
        configureCollectionView()
    }
    
    func configureCollectionView() {
        
        collectionView.do {
            $0.register(cellType: ActionCollectionViewCell.self)
            $0.allowsMultipleSelection = false
            $0.showsHorizontalScrollIndicator = false
            $0.collectionViewLayout = configureCollectionViewLayout()
        }
        
        func configureCollectionViewLayout() -> UICollectionViewFlowLayout {
            let layout = UICollectionViewFlowLayout().then {
                $0.itemSize = CGSize(width: UIScreen.main.bounds.width - 32, height: 145)
                $0.minimumInteritemSpacing = 10
                $0.minimumLineSpacing = 10
                $0.scrollDirection = .horizontal
            }
            
            return layout
            
        }
    }
}

private extension ActionCarouselTableViewCell {
    func bindUI() {
        bindViewModel()
        bindCollectionView()
    }
    
    func bindViewModel() {
        
        let selectedModel = collectionView.rx.modelSelected(ActionCarouselCellModelProtocol.self).asObservable()
        input = ActionCarouselTableViewCellViewModelInput(selectedModel: selectedModel)
        output = viewModel.transform(input: input)
    }
    
    func bindCollectionView() {
        output.items.drive(collectionView.rx.items(cellIdentifier: ActionCollectionViewCell.reuseIdentifier,
                                                   cellType: ActionCollectionViewCell.self)) { _, model, cell in
            cell.configureCell(with: model)
        }
        .disposed(by: disposeBag)
    }
}
