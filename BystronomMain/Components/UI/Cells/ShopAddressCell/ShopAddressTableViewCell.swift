//
//  ShopAddressTableViewCell.swift
//  BystronomMain
//
//  Created by Alex Peredreev on 20.07.2021.
//

import UIKit
import RxSwift
import RxCocoa
import RxGesture

final class ShopAddressTableViewCell: UITableViewCell, CellConfigurable {
    
    private var disposeBag = DisposeBag()

    @IBOutlet private weak var iconImageView: UIImageView!
    @IBOutlet private weak var addressLabel: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configureCell(with model: ShopAddressCellModelProtocol) {
        disposeBag = DisposeBag()
        
        addressLabel.setTitle(model.title, for: .normal)
        addressLabel.rx.tap
            .bind(to: model.addressTap)
            .disposed(by: disposeBag)
        
    }
    
}

private extension ShopAddressTableViewCell {
    
    func configureUI() {
        configureViews()
        configureButton()
    }
    
    func configureViews() {
        iconImageView.image = UIImage(named: "location")
    }
    
    func configureButton() {
        addressLabel.tintColor = .blue
    }
}
